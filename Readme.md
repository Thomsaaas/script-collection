# Script-Collection

## pdf-generator

Dependencies:
```
imagemagick, ghostscript
```

This utility creates a pdf from all jpg and png files and compresses the pdf.
The uncompressed version gets deleted. Note that the images will be ordered
alphabetically, although jpg and png will be treated equally because all jpgs
will be converted to pngs before.

## wallpaper-selector

Dependencies:
```
xwallpaper, nsxiv
```

This utility opens your /usr/share/backgrounds directory and displays all files
in nsxiv's thumbnail mode. You can now select an image with 'm' and quit the
application to set the wallpaper.

## minecraft-launcher

Dependencies:
```
Prismlauncher, dmenu
```
This utility lists out all Minecraft-instances in the ~/.local/share/Prismlauncher/instances 
directory and lets you select one to launch.
