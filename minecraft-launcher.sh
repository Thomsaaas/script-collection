#!/usr/bin/env sh

prismlauncher -l $(ls ~/.local/share/PrismLauncher/instances/ | \
    dmenu -fn 'Cascadia Code-12' -nb '#1E1D2D' -nf '#FAE380' -sb '#1E1D2D' \
    -i -c -l 5) &
