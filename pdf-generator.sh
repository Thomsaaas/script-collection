#!/usr/bin/env sh

for i in $(pwd)/*.jpg ; do convert "$i" "${i%.*}.png" ; done

convert $(pwd)/*.png output.pdf ;

gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/default \
    -dNOPAUSE -dQUIET -dBATCH -dCompressFonts=true -r150 \
    -sOutputFile=$(date "+%d.%m.%Y_%T").pdf \
    output.pdf ;
    rm output.pdf
